<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class actionController extends Controller
{
 public function showActions(){

 	 $table = DB::table('actions')->get();
 	return view('index',compact('table'));
 }
 public function getAction($id){
 	$action = DB::table('actions')->where('id','=',$id)->first();
 	$allRates = DB::table('rating')
 	->join('actions','actions.id','=','rating.action_id')
 	->where('actions.id','=',$id)
 	->select('rating.rating')
 	->get();

$rates=0;
$sum=0;
 	foreach ($allRates as $value) {
 		$sum+=$value->rating;
 		$rates++;
 	}
$rate = round($sum / $rates);
 	
 	return view('template',compact('action','id','rate'));
 }
public function storeRate(Request $req, $id){
	
 DB::table('rating')->insert(
['rating'=>($req->rate) ,'action_id'=>$id ] );
return redirect()->route('showAction',compact('id'));
}

}

