<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'actionController@showActions');
Route::get('/action/{id}','actionController@getAction')->name('showAction');
Route::post('/action/{id}','actionController@storeRate')->name('storeRate');
/*Route::get('/sh1', function () {
    return view('sh1');
});
Route::get('/sh2', function () {
    return view('sh2');
});*/
Route::get('/index', function () {
    return view('index');
});
