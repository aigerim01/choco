<!DOCTYPE html>
<html lang="ru">
<head>
<title></title>
<meta charset="utf-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen">
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/cufon-replace.js" type="text/javascript"></script>
<script src="js/Open_Sans_400.font.js" type="text/javascript"></script>
<script src="js/Open_Sans_Light_300.font.js" type="text/javascript"></script> 
<script src="js/Open_Sans_Semibold_600.font.js" type="text/javascript"></script>  
<script type="text/javascript" src="js/tms-0.3.js"></script>
<script type="text/javascript" src="js/tms_presets.js"></script> 
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script> 
<script src="js/FF-cash.js" type="text/javascript"></script>
<!--[if lt IE 7]>
	<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg" border="0"  alt="" /></a>
	</div>
<![endif]-->
<!--[if lt IE 9]>
	<script type="text/javascript" src="js/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>
<body id="page1">
<!-- header -->
	<div class="bg">
		<div class="main">
			<header>
				<div class="row-1">
					<h1>
						<a class="logo" href="index">Chocolife.me</a>
						<strong class="slog">The main thing that you were happy!</strong>
					</h1>
					<form id="search-form" action="" method="post" enctype="multipart/form-data">
						<fieldset>
							<div class="search-form">					
								<input type="text" name="search" value="Find among the 624 shares" onBlur="if(this.value=='') this.value='Type Keyword Here'" onFocus="if(this.value =='Type Keyword Here' ) this.value=''" />
								<a href="#" onClick="document.getElementById('search-form').submit()">Search</a>									
							</div>
						</fieldset>
					</form>
				</div>
				<ul id="navbar">
      <li><a href="#">All</a></li>
      <li><a href="#">New</a></li>
      <li><a href="#">Bestsellers</a></li>
      <li><a href="#">Entertainment</a></li>
	   <li><a href="#">Sport</a></li>
	    <li><a href="#">Beuty and health</a></li>
		 <li><a href="#">Food</a></li>
		  <li><a href="#">Tourism</a></li>
    </ul>
	<p><img src="images/a.jpg"width="1000" alt=""></p>
				<div class="row-2">
					<nav>
						<ul class="menu">
						 <li><a class="active" href="index.html">Shares</a></li>
						  <li><a href="news.html">Catalog</a></li>
						  <li><a href="services.html">Shipping and payment</a></li>
						  <li><a href="products.html">Bonuses</a></li>
						  <li class="last-item"><a href="contacts.html">Contact Us</a></li>
						</ul>
					</nav>
				</div>
				<div class="row-3">
					<div class="slider-wrapper">
						<div class="slider">
						
						  <ul class="items">
							<li><img src="images/slider-img1.jpg" alt="">
								<strong class="banner">
									<strong class="b1">MEDEO</strong>
									<strong class="b2">-30%</strong>
									<strong class="b3">Ride for your own pleasure!</strong>
								</strong>
							</li>
							<li><img src="images/slider-img2.jpg" alt="">
								<strong class="banner">
									<strong class="b1">TAU SPA CENTER</strong>
									<strong class="b2">-61%</strong>
									<strong class="b3">Now even cheaper!</strong>
								</strong>
							</li>
							<li><img src="images/slider-img3.jpg" alt="">
								<strong class="banner">
									<strong class="b1">TABAGAN</strong>
									<strong class="b2">-55%</strong>
									<strong class="b3">Accommodation in a ski resort</strong>
								</strong>
							</li>
						  </ul>
						  <a class="prev" href="#">prev</a>
						  <a class="next" href="#">prev</a>
						</div>
					</div>
				</div>
				
				
			</header>
<!-- content -->
			<section id="content">
				<div class="padding">
						<div class="wrapper">
<?php
$counter =0;
 ?>
						@foreach($table as $action)
						@if($counter%3==0)
							</div>
							</div>
							</section>
							<section id="content">
				<div class="padding">
						<div class="wrapper">
						@endif


							<div class="col-1">
								<div class="box first">
									<div class="pad">
										<div class="wrapper indent-bot">
									
											<div class="extra-wrap">
												<img src="{{$action->image}}" alt="">
											</div>
										</div>
										<div class="wrapper">
											<a class="button img-indent-r" href="/action/{{$action->id}}"></a>
											<div class="extra-wrap">
											<p>{{$action-> head}}</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							@endforeach
<!--

							<div class="col-1">
								<div class="box second">
									<div class="pad">
										<div class="wrapper indent-bot">
											
											<div class="extra-wrap">
											<img src="images/2.JPG" alt="">
										</div>
										</div>
										<div class="wrapper">
											<a class="button img-indent-r" href="sh2"></a>
											<div class="extra-wrap">
												<p>Accommodation in the best rooms for 1 day</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-2">
								<div class="box third">
									<div class="pad">
										<div class="wrapper indent-bot">
									
											<div class="extra-wrap">
												<img src="images/3.JPG" alt="photo">
											</div>
										</div>
										<div class="wrapper">
											<a class="button img-indent-r" href="#">>></a>
											<div class="extra-wrap">
												<p>Styling,various types of male-up and eyebrow dyeing</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<section id="content">
				<div class="padding">
						<div class="wrapper">
							<div class="col-1">
								<div class="box first">
									<div class="pad">
										<div class="wrapper indent-bot">
									
											<div class="extra-wrap">
												<img src="images/4.JPG" alt="">
											</div>
										</div>
										<div class="wrapper">
											<a class="button img-indent-r" href="sh1">>></a>
											<div class="extra-wrap">
											<p>Аll menus with 50% discount in Greek cuisine</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-1">
								<div class="box second">
									<div class="pad">
										<div class="wrapper indent-bot">
											
											<div class="extra-wrap">
											<img src="images/5.JPG" alt="">
											</div>
										</div>
										<div class="wrapper">
											<a class="button img-indent-r" href="sh2.html"></a>
											<div class="extra-wrap">
												<p>Еntry on weekdays and weekends</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-2">
								<div class="box third">
									<div class="pad">
										<div class="wrapper indent-bot">
									
											<div class="extra-wrap">
												<img src="images/6.JPG" alt="">
											</div>
										</div>
										<div class="wrapper">
											<a class="button img-indent-r" href="#">>></a>
											<div class="extra-wrap">
												<p>15% discount on the menu!<p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
							<section id="content">
				<div class="padding">
						<div class="wrapper">
							<div class="col-1">
								<div class="box first">
									<div class="pad">
										<div class="wrapper indent-bot">
									
											<div class="extra-wrap">
												<img src="images/7.JPG" alt="">
											</div>
										</div>
										<div class="wrapper">
											<a class="button img-indent-r" href="sh1.html">>></a>
											<div class="extra-wrap">
											<p>2 tickets for any session + popcorn + 2 codes</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-1">
								<div class="box second">
									<div class="pad">
										<div class="wrapper indent-bot">
											
											<div class="extra-wrap">
											<img src="images/8.JPG" alt="">
											</div>
										</div>
										<div class="wrapper">
											<a class="button img-indent-r" href="sh2.html"></a>
											<div class="extra-wrap">
												<p>Amusement Rides, Slot Machines</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-2">
								<div class="box third">
									<div class="pad">
										<div class="wrapper indent-bot">
									
											<div class="extra-wrap">
												<img src="images/9.JPG" alt="">
											</div>
										</div>
										<div class="wrapper">
											<a class="button img-indent-r" href="#">>></a>
											<div class="extra-wrap">
											<p>	1,2,3 months of visiting the pool for adolescents and children<p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
							<div class="col-1">
								<div class="box first">
									<div class="pad">
										<div class="wrapper indent-bot">
									
											<div class="extra-wrap">
												<img src="images/10.JPG" alt="">
											</div>
										</div>
										<div class="wrapper">
											<a class="button img-indent-r" href="sh1.html">>></a>
											<div class="extra-wrap">
											Give happiness to children!
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-1">
								<div class="box second">
									<div class="pad">
										<div class="wrapper indent-bot">
											
											<div class="extra-wrap">
											<img src="images/11.JPG" alt="">
											</div>
										</div>
										<div class="wrapper">
											<a class="button img-indent-r" href="sh2.html"></a>
											<div class="extra-wrap">
												40% discount for lunches and dinners
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-2">
								<div class="box third">
									<div class="pad">
										<div class="wrapper indent-bot">
									
											<div class="extra-wrap">
												<img src="images/12.JPG" alt="">
											</div>
										</div>
										<div class="wrapper">
											<a class="button img-indent-r" href="#">>></a>
											<div class="extra-wrap">
												Вiscount on doctor's appointment
										</div>
									</div>
								</div>
							</div>
						</div>
					</div> -->
					
							
					<div class="wrapper">
						
						</div>
						
			</section>
<!-- footer -->
		<footer>
				<div class="row-top">
					<div class="row-padding">
						<div class="wrapper">
							<div class="col-1">
								<h4>Address:</h4>
								<dl class="address">
									<dt><span>Country:</span>Qazakstan</dt>
									<dd><span>City:</span>Almaty</dd>
								
									<dd><span>Email:</span><a href="#">aikaku@mail.ru</a></dd>
								</dl>
							</div>
							<div class="col-2">
								<h4>Follow Us:</h4>
								<ul class="list-services">
									<li class="item-1"><a href="#">Facebook</a></li>
									<li class="item-2"><a href="#">Twitter</a></li>
									<li class="item-3"><a href="#">Linkedin</a></li>
								</ul>
							</div>
							
							<div class="col-1">
								<div class="indent3">
									<strong class="footer-logo">Chocolife.me</strong>
									<strong class="phone"><strong>Call center:</strong> 87788812811</strong>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				</div>
			</footer>
		</div>
	</div>
	<script type="text/javascript"> Cufon.now(); </script>
	<script type="text/javascript">
		$(function(){
			$('.slider')._TMS({
				prevBu:'.prev',
				nextBu:'.next',
				playBu:'.play',
				duration:800,
				easing:'easeOutQuad',
				preset:'simpleFade',
				pagination:false,
				slideshow:3000,
				numStatus:false,
				pauseOnHover:true,
				banners:true,
				waitBannerAnimation:false,
				bannerShow:function(banner){
					banner
						.hide()
						.fadeIn(500)
				},
				bannerHide:function(banner){
					banner
						.show()
						.fadeOut(500)
				}
			});
		})
	</script>
</body>
</html>
