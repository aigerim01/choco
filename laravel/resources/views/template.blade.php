<!DOCTYPE html>
<html lang="ru">
<head>
<title>{{$action->head}}</title>
<meta charset="utf-8">
<link rel="stylesheet" href="{{URL::asset('css/reset.css') }}" type="text/css" media="screen">
<link rel="stylesheet" href="{{URL::asset('css/style.css') }}" type="text/css" media="screen">
<link rel="stylesheet" href="{{URL::asset('css/layout.css') }}" type="text/css" media="screen">
<script type="text/javascript" src="{{URL::asset('js/jquery-1.6.min.js') }}"></script>
<script src="{{URL::asset('js/cufon-yui.js') }}" type="text/javascript"></script>
<script src="{{URL::asset('js/cufon-replace.js') }}" type="text/javascript"></script>
<script src="{{URL::asset('js/Open_Sans_400.font.js') }}" type="text/javascript"></script>
<script src="{{URL::asset('js/Open_Sans_Light_300.font.js') }}" type="text/javascript"></script> 
<script src="{{URL::asset('js/Open_Sans_Semibold_600.font.js') }}" type="text/javascript"></script>  
<script type="text/javascript" src="{{URL::asset('js/tms-0.3.js') }}"></script>
<script type="text/javascript" src="{{URL::asset('js/tms_presets.js') }}"></script> 
<script type="text/javascript" src="{{URL::asset('js/jquery.easing.1.3.js') }}"></script> 
<script src="{{URL::asset('js/FF-cash.js') }}" type="text/javascript"></script>

</head>
<body id="page1">
<!-- header -->
	<div class="bg">
		<div class="main">
			<header>
			
				<div class="row-1">
					<h1>
						<a class="logo" href="index">Chocolife.me</a>
						<strong class="slog">The main thing that you were happy!</strong>
					</h1>
					<form id="search-form" action="" method="post" enctype="multipart/form-data">
						<fieldset>
							<div class="search-form">					
								<input type="text" name="search" value="Find among the 624 shares" onBlur="if(this.value=='') this.value='Type Keyword Here'" onFocus="if(this.value =='Type Keyword Here' ) this.value=''" />
								<a href="#" onClick="document.getElementById('search-form').submit()">Search</a>									
							</div>
						</fieldset>
					</form>
				</div>
				<ul id="navbar">
      <li><a href="#">All</a></li>
      <li><a href="#">New</a></li>
      <li><a href="#">Bestsellers</a></li>
      <li><a href="#">Entertainment</a></li>
	   <li><a href="#">Sport</a></li>
	    <li><a href="#">Beuty and health</a></li>
		 <li><a href="#">Food</a></li>
		  <li><a href="#">Tourism</a></li>
    </ul>
	<p><img src=""width="1000" alt=""></p>
				<div class="row-2">
					<nav>
						<ul class="menu">
						  <li><a class="active" href="index">Shares</a></li>
						  <li><a href="news.html">Catalog</a></li>
						  <li><a href="services.html">Shipping and payment</a></li>
						  <li><a href="products.html">Bonuses</a></li>
						  <li class="last-item"><a href="contacts.html">Contact Us</a></li>
						</ul>
					</nav>
				</div>
				
			</header>
			<section id="content">
				<div class="padding">
						<div class="wrapper">
			
									<!--Основная часть-->
				<div class="main"><br>
					<h8>Information</h8>
					<h5>Rating:{{$rate}}</h5>
					<form name="rate" method="post" action="{{ route('storeRate',compact('id')) }}">
						{{ csrf_field() }}
						<input type="radio" name="rate" value='1'>1</input>
						<input type="radio" name="rate" value='2'>2</input>
						<input type="radio" name="rate" value='3'>3</input>
						<input type="radio" name="rate" value='4'>4</input>
						<input type="radio" name="rate" value='5'>5</input>
						<button type="submit">rate</button>
					</form>

					<p> email: <a href="#" title="link" >fotossesion.kz.</a></p>
                    <br><b><h7>{{$action->head}}</h7></b></br> <img src="/{{$action->image}}"height="300" width="300" style="float:left;margin:10px;">

<p>{{$action->info}}</p>
                    </div>
				<style>
				.rightimg{
				float:right;
				margin:7px 0px 7px 7px;
			}
			h9{
			font-family:serfit;
			font-size:200%;
			color:#000000;
		margin-left:10%
			}
			h10{
			font-family:serfit;
			font-size:200%;
			color:#000000;
		margin-left:55%;
			}
			h11{
			font-family:serfit;
			font-size:200%;
			color:#000000;
		
			}
				</style>
				
				</div>
			</div>
			</section id="content">
				</div class="padding">
						</div class="wrapper">
<!--Footer-->
			<footer>
				<div class="row-top">
					<div class="row-padding">
						<div class="wrapper">
							<div class="col-1">
								<h4>Address:</h4>
								<dl class="address">
									<dt><span>Country:</span>Qazakstan</dt>
									<dd><span>City:</span>Almaty</dd>
								
									<dd><span>Email:</span><a href="#">aikaku@mail.ru</a></dd>
								</dl>
							</div>
							<div class="col-2">
								<h4>Follow Us:</h4>
								<ul class="list-services">
									<li class="item-1"><a href="#">Facebook</a></li>
									<li class="item-2"><a href="#">Twitter</a></li>
									<li class="item-3"><a href="#">Linkedin</a></li>
								</ul>
							</div>
							<div class="col-3">
								<h4>Why Us:</h4>
								<ul class="list-1">
									<li><a href="#">The best institutions</a></li>
									<li><a href="#">Security</a></li>
									<li><a href="#">Cheap</a></li> 
									<li><a href="#">Quality control</a></li>
								</ul>
							</div>
							<div class="col-4">
								<div class="indent3">
									<strong class="footer-logo">Chocolife.me</strong>
									<strong class="phone"><strong>Call center:</strong> 87788812811</strong>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				</div>
			</footer>
		</div>
	</div>
	<script type="text/javascript"> Cufon.now(); </script>
	<script type="text/javascript">
		$(function(){
			$('.slider')._TMS({
				prevBu:'.prev',
				nextBu:'.next',
				playBu:'.play',
				duration:800,
				easing:'easeOutQuad',
				preset:'simpleFade',
				pagination:false,
				slideshow:3000,
				numStatus:false,
				pauseOnHover:true,
				banners:true,
				waitBannerAnimation:false,
				bannerShow:function(banner){
					banner
						.hide()
						.fadeIn(500)
				},
				bannerHide:function(banner){
					banner
						.show()
						.fadeOut(500)
				}
			});
		})
	</script>
</body>
</html>

